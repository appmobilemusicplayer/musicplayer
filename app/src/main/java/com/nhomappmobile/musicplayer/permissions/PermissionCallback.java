package com.nhomappmobile.musicplayer.permissions;

public interface PermissionCallback {
    void permissionGranted();

    void permissionRefused();
}