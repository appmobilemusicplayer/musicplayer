package com.nhomappmobile.musicplayer.listeners;


public interface MusicStateListener {

    /**
     * Called when {@link com.nhomappmobile.musicplayer.MusicService#REFRESH} is invoked
     */
    void restartLoader();

    /**
     * Called when {@link com.nhomappmobile.musicplayer.MusicService#PLAYLIST_CHANGED} is invoked
     */
    void onPlaylistChanged();

    /**
     * Called when {@link com.nhomappmobile.musicplayer.MusicService#META_CHANGED} is invoked
     */
    void onMetaChanged();

}
