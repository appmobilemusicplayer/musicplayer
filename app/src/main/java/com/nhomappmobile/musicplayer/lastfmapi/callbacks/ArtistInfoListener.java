package com.nhomappmobile.musicplayer.lastfmapi.callbacks;
import com.nhomappmobile.musicplayer.lastfmapi.models.LastfmArtist;
public interface ArtistInfoListener {
    public void artistInfoSucess(LastfmArtist artist);
    public void artistInfoFailed();
}
