package com.nhomappmobile.musicplayer.lastfmapi.callbacks;

public interface UserListener {
    void userSuccess();

    void userInfoFailed();

}
