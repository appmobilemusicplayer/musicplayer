package com.nhomappmobile.musicplayer.lastfmapi.callbacks;
import com.nhomappmobile.musicplayer.lastfmapi.models.LastfmAlbum;
public interface AlbuminfoListener {
    public void albumInfoSucess(LastfmAlbum album);
    public void albumInfoFailed();
}

