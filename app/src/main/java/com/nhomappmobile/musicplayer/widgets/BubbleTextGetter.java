package com.nhomappmobile.musicplayer.widgets;

public interface BubbleTextGetter {
    String getTextToShowInBubble(int pos);
}